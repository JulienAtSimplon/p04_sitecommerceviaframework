import Vue from 'vue'
import VueRouter from 'vue-router';
// Lib
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import 'bootstrap/dist/css/bootstrap.css';
// Components
import App from './App.vue';
import Home from './components/Home.vue';
import Details from './components/Details.vue'
import Cart from './components/Cart.vue'

// Settings
Vue.use(VueRouter);
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);

Vue.config.productionTip = false;

const routes = [
	{ path: `/`, 			redirect: '/home' },
	{ path: `/home`, 		name:"home",	component: Home },
	{ path: `/details/:id`, name:"details",	component: Details },
	{ path: `/cart`, 		name:"cart",	component: Cart },
]

const router = new VueRouter({ routes })

new Vue({
	router,
	render: h => h(App),
}).$mount('#app')